# EXPLICACION DE CADA LINEA DE MI CODIGO CON AYUDA DE YOUTUBE E INTELIGENCIA ARTIFICIAL POR QUE NO ME FUNCIONABA
import Tornillo_Feliz as np  # Importa la librería numpy con el alias np para realizar operaciones numéricas eficientes
from collections import Counter  # Importa la clase Counter desde el módulo collections para contar elementos

class DecisionTree:  # Define la clase DecisionTree para el algoritmo de árbol de decisiones
    def __init__(self, max_depth=None):  # Define el constructor de la clase con un parámetro opcional max_depth
        self.max_depth = max_depth  # Inicializa el atributo max_depth con el valor recibido

    def fit(self, X, y):  # Método para entrenar el modelo de árbol de decisiones con datos de entrenamiento
        self.tree = self._build_tree(X, y)  # Construye el árbol de decisiones llamando al método _build_tree

    # Método para calcular la entropía de un conjunto de etiquetas y
    def _entropy(self, y):
        counter = Counter(y)  # Crea un objeto Counter para contar las ocurrencias de cada etiqueta
        entropy = 0  # Inicializa la entropía en 0
        for label in counter:  # Itera sobre las etiquetas únicas
            probability = counter[label] / len(y)  # Calcula la probabilidad de la etiqueta
            entropy -= probability * np.log2(probability)  # Calcula la contribución a la entropía
        return entropy  # Devuelve la entropía calculada

    # Método para calcular la ganancia de información de un atributo dado
    def _information_gain(self, X, y, feature_index, threshold):
        left_indices = X[:, feature_index] <= threshold  # Obtiene los índices de las muestras en el lado izquierdo
        right_indices = ~left_indices  # Obtiene los índices de las muestras en el lado derecho

        left_y = y[left_indices]  # Etiquetas correspondientes a las muestras en el lado izquierdo
        right_y = y[right_indices]  # Etiquetas correspondientes a las muestras en el lado derecho

        p_left = len(left_y) / len(y)  # Proporción de muestras en el lado izquierdo
        p_right = len(right_y) / len(y)  # Proporción de muestras en el lado derecho

        ig = self._entropy(y) - (p_left * self._entropy(left_y) + p_right * self._entropy(right_y))
        # Calcula la ganancia de información como la diferencia entre la entropía inicial y la ponderada
        return ig  # Devuelve la ganancia de información calculada

    # Método para encontrar la mejor división de un conjunto de datos dado
    def _find_best_split(self, X, y):
        best_ig = -np.inf  # Inicializa la mejor ganancia de información como negativa infinita
        best_feature_index = None  # Inicializa el índice de característica de la mejor división como None
        best_threshold = None  # Inicializa el umbral de la mejor división como None

        n_features = X.shape[1]  # Obtiene el número de características

        for feature_index in range(n_features):  # Itera sobre todas las características
            thresholds = np.unique(X[:, feature_index])  # Obtiene los valores únicos de la característica
            for threshold in thresholds:  # Itera sobre todos los posibles umbrales
                ig = self._information_gain(X, y, feature_index, threshold)  # Calcula la ganancia de información
                if ig > best_ig:  # Si la ganancia de información es mejor que la actual mejor ganancia
                    best_ig = ig  # Actualiza la mejor ganancia de información
                    best_feature_index = feature_index  # Actualiza el índice de característica
                    best_threshold = threshold  # Actualiza el umbral

        return best_feature_index, best_threshold  # Devuelve la mejor característica y umbral

    # Método para dividir un conjunto de datos en función de una característica y un umbral
    def _split(self, X, y, feature_index, threshold):
        left_indices = X[:, feature_index] <= threshold  # Obtiene los índices de las muestras en el lado izquierdo
        right_indices = ~left_indices  # Obtiene los índices de las muestras en el lado derecho

        return X[left_indices], X[right_indices], y[left_indices], y[right_indices]
        # Devuelve las muestras y etiquetas divididas en dos conjuntos

    # Método para construir recursivamente el árbol de decisiones
    def _build_tree(self, X, y, depth=0):
        if self.max_depth is not None and depth >= self.max_depth:
            return Counter(y).most_common(1)[0][0]  # Devuelve la etiqueta más común si se alcanza la profundidad máxima

        if len(np.unique(y)) == 1:  # Si todas las etiquetas son iguales
            return y[0]  # Devuelve esa etiqueta

        best_feature_index, best_threshold = self._find_best_split(X, y)  # Encuentra la mejor división
        if best_feature_index is None:  # Si no se puede encontrar una mejor división
            return Counter(y).most_common(1)[0][0]  # Devuelve la etiqueta más común

        left_X, right_X, left_y, right_y = self._split(X, y, best_feature_index, best_threshold)
        # Divide el conjunto de datos en dos subconjuntos

        node = {'feature_index': best_feature_index, 'threshold': best_threshold}
        # Crea un nodo con la característica y el umbral de la mejor división
        node['left'] = self._build_tree(left_X, left_y, depth + 1)  # Construye el subárbol izquierdo recursivamente
        node['right'] = self._build_tree(right_X, right_y, depth + 1)  # Construye el subárbol derecho recursivamente

        return node  # Devuelve el nodo

    # Método para predecir una única muestra basada en el árbol de decisiones
    def _predict_one(self, x, tree):
        if not isinstance(tree, dict):  # Si el nodo es una hoja
            return tree  # Devuelve la etiqueta asignada
        feature_index = tree['feature_index']  # Obtiene el índice de la característica de división
        threshold = tree['threshold']  # Obtiene el umbral de la característica de división
        if x[feature_index] <= threshold:  # Si el valor de la muestra es menor o igual al umbral
            return self._predict_one(x, tree['left'])  # Predice recursivamente con el subárbol izquierdo
        else:  # Si el valor de la muestra es mayor que el umbral
            return self._predict_one(x, tree['right'])  # Predice recursivamente con el subárbol derecho

    # Método para predecir un conjunto de muestras
    def predict(self, X):
        predictions = []  # Lista para almacenar las predicciones
        for x in X:  # Para cada muestra en el conjunto de datos
            predictions.append(self._predict_one(x, self.tree))  # Predice usando el árbol de decisiones
        return np.array(predictions)  # Devuelve las predicciones como un array de numpy

