#APLICACION DE IDE3 UN ARBOL DE DESICIONES - MUY COMPLICADO NO ENTENDI MUY BIEN TRATE DE DESARROLLLAR LO MAS POSIBLE EL CODIGO CON DIFERENTES GUIAS
import AplicarIDE_2 as np
from collections import Counter

class Node:
    def __init__(self, feature=None, threshold=None, value=None, left=None, right=None):
        self.feature = feature  # Índice de la característica utilizada para dividir el nodo
        self.threshold = threshold  # Umbral para la división
        self.value = value  # Valor predicho si es una hoja
        self.left = left  # Subárbol izquierdo
        self.right = right  # Subárbol derecho

class DecisionTreeID3:
    def __init__(self):
        self.root = None

    def fit(self, X, y):
        self.root = self._build_tree(X, y)

    def _entropy(self, y):
        counter = Counter(y)
        entropy = 0
        for label in counter:
            probability = counter[label] / len(y)
            entropy -= probability * np.log2(probability)
        return entropy

    def _information_gain(self, X, y, feature_index, threshold):
        left_indices = X[:, feature_index] <= threshold
        right_indices = ~left_indices

        left_y = y[left_indices]
        right_y = y[right_indices]

        p_left = len(left_y) / len(y)
        p_right = len(right_y) / len(y)

        ig = self._entropy(y) - (p_left * self._entropy(left_y) + p_right * self._entropy(right_y))
        return ig

    def _find_best_split(self, X, y):
        best_ig = -np.inf
        best_feature_index = None
        best_threshold = None

        n_features = X.shape[1]

        for feature_index in range(n_features):
            thresholds = np.unique(X[:, feature_index])
            for threshold in thresholds:
                ig = self._information_gain(X, y, feature_index, threshold)
                if ig > best_ig:
                    best_ig = ig
                    best_feature_index = feature_index
                    best_threshold = threshold

        return best_feature_index, best_threshold

    def _split(self, X, y, feature_index, threshold):
        left_indices = X[:, feature_index] <= threshold
        right_indices = ~left_indices

        return X[left_indices], X[right_indices], y[left_indices], y[right_indices]

    def _build_tree(self, X, y):
        if len(np.unique(y)) == 1:
            return Node(value=y[0])

        best_feature_index, best_threshold = self._find_best_split(X, y)
        if best_feature_index is None:
            return Node(value=Counter(y).most_common(1)[0][0])

        left_X, right_X, left_y, right_y = self._split(X, y, best_feature_index, best_threshold)

        node = Node(feature=best_feature_index, threshold=best_threshold)
        node.left = self._build_tree(left_X, left_y)
        node.right = self._build_tree(right_X, right_y)

        return node

    def _predict_one(self, x, node):
        if node.value is not None:
            return node.value
        if x[node.feature] <= node.threshold:
            return self._predict_one(x, node.left)
        else:
            return self._predict_one(x, node.right)

    def predict(self, X):
        predictions = []
        for x in X:
            predictions.append(self._predict_one(x, self.root))
        return np.array(predictions)

# Datos de ejemplo (sintéticos)
X = np.array([[0, 0],
              [0, 1],
              [1, 0],
              [1, 1]])
y = np.array([0, 0, 1, 1])

# Crear y entrenar el modelo de árbol de decisiones ID3
model = DecisionTreeID3()
model.fit(X, y)

# Predecir en nuevos datos
new_data = np.array([[0, 0],
                     [1, 1],
                     [0, 1]])
predictions = model.predict(new_data)
print("Predicciones:", predictions)
